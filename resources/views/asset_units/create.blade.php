@extends('layouts.app')

@section('content')

	<section class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<h1>Add Item</h1>
				@if ($errors->any())
					@foreach($errors->all() as $error)
						<div class="alert alert-danger">
							<small><p class="">{{$error}}</p></small>
						</div>
					@endforeach
				@endif
				<form method="POST" enctype="multipart/form-data" action="{{route('asset_unit.store')}}">
					@csrf
					<div class="form-group">
						<label for="name">Name:</label>
						<input type="text" name="name" id="name" class="form-control">
					</div>
					<div class="form-group">
						<label for="category">Category:</label>
						<select class="form-control" id="category" name="category">
							<option value="1">Category 1</option>
						</select>
					</div>

					<div class="form-group">
						<label for="description">Description:</label>
						<textarea name="description" id="description" class="form-control"></textarea>
					</div>
					<div class="form-group">
						<label for="image">Image:</label>
						<input type="file" name="image" id="image" class="form-control-file">
					</div>
					<button class="btn btn-primary btn-block">Add Item</button>
				</form>

			</div>
		</div>
		
	</section>
@endsection