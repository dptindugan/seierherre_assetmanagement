@extends('layouts.app')

@section('content')

	<section class="container">
		<div class="row">
			
			<div class="col-12 col-md-8 mx-auto">
				{{$asset_Unit}}

				<div class="card">
					<img src="{{url('/public/'.$asset_Unit->images)}}">
					
				</div>
				
			</div>
			
		</div>
		
	</section>

@endsection