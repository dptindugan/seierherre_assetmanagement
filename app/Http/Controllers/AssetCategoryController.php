<?php

namespace App\Http\Controllers;

use App\Asset_Category;
use Illuminate\Http\Request;

class AssetCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset_Category  $asset_Category
     * @return \Illuminate\Http\Response
     */
    public function show(Asset_Category $asset_Category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset_Category  $asset_Category
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset_Category $asset_Category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset_Category  $asset_Category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset_Category $asset_Category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset_Category  $asset_Category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset_Category $asset_Category)
    {
        //
    }
}
