<?php

namespace App\Http\Controllers;

use App\Asset_Unit;
use Illuminate\Http\Request;
use Str;
use App\Asset_Category;

class AssetUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('asset_units.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image'=> 'required|image|max:3000'
        ]);

        // echo "Hello World";

        $file = $request->file('image');


        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $file_extension = $file->extension();

        $random_chars = Str::random(10);

        $new_file_name = date('Y-m-d-H-i-s')."_".$random_chars."_".$file_name.".".$file_extension;


        $filepath = $file->storeAs('images',$new_file_name,'public');

        $data = $request->all(
        'name',
        'category',
        'description'
        );

        extract($data);

        $assets = new Asset_Unit;
        $assets->name = $name;
        $assets->category_id = $category;
        $assets->description = $description;
        $assets->images = $filepath;
        $assets->save();


        // Asset_Unit::create([
        //     'name' => $name,
        //     'category_id' => $category,
        //     'description' => $description,
        //     'images' => $filepath
        // ]);

        // $assets = new Asset_Unit;

        return redirect(route('asset_unit.show',['asset_unit'=> $assets->id]));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset_Unit  $asset_Unit
     * @return \Illuminate\Http\Response
     */
    public function show(Asset_Unit $asset_unit)
    {
        // Asset_unit::all();
        dd($asset_unit);
        // return view('asset_units.show')->with('asset_unit', $asset_unit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset_unit  $asset_unit
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset_unit $asset_unit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset_unit  $asset_unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset_unit $asset_unit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset_unit  $asset_unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset_unit $asset_unit)
    {
        //
    }
}
