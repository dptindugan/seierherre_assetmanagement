<?php

namespace App\Http\Controllers;

use App\demand_statuses;
use Illuminate\Http\Request;

class DemandStatusesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\demand_statuses  $demand_statuses
     * @return \Illuminate\Http\Response
     */
    public function show(demand_statuses $demand_statuses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\demand_statuses  $demand_statuses
     * @return \Illuminate\Http\Response
     */
    public function edit(demand_statuses $demand_statuses)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\demand_statuses  $demand_statuses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, demand_statuses $demand_statuses)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\demand_statuses  $demand_statuses
     * @return \Illuminate\Http\Response
     */
    public function destroy(demand_statuses $demand_statuses)
    {
        //
    }
}
