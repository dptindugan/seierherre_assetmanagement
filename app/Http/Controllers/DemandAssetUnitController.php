<?php

namespace App\Http\Controllers;

use App\demand_assetUnit;
use Illuminate\Http\Request;

class DemandAssetUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\demand_assetUnit  $demand_assetUnit
     * @return \Illuminate\Http\Response
     */
    public function show(demand_assetUnit $demand_assetUnit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\demand_assetUnit  $demand_assetUnit
     * @return \Illuminate\Http\Response
     */
    public function edit(demand_assetUnit $demand_assetUnit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\demand_assetUnit  $demand_assetUnit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, demand_assetUnit $demand_assetUnit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\demand_assetUnit  $demand_assetUnit
     * @return \Illuminate\Http\Response
     */
    public function destroy(demand_assetUnit $demand_assetUnit)
    {
        //
    }
}
