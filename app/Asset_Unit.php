<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asset_Unit extends Model
{
	use SoftDeletes;
    public function asset_category(){
    	return $this->belongsTo('App\Asset_Category');
    }

    protected $fillable = ['name', 'description', 'category_id', 'images'];

}
