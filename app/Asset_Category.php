<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asset_Category extends Model
{
	use SoftDeletes;
    public function asset_units(){
    	return $this->hasMany('App\Asset_Unit');
    }
}
