<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class demand extends Model
{

    public function user(){
		return $this->belongsTo('App\User');
	}
	public function asset_category_id(){
		return $this->belongsTo('App\Asset_Category');
	}
	public function status(){
		return $this->belongsTo('App\demand_status');
	}

	public function asset_units(){
		return $this->belongsToMany('App\Product','demand_assetUnit')
			->withPivot('category_id','name','stocks','serial_number')
			->withTimestamps();
	}   
}
