<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDemandAssetUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demand_asset_units', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('demands_id');
            $table->foreign('demands_id')
                    ->references('id')
                    ->on('demands')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
            $table->unsignedBigInteger('asset_unit_id');
            $table->foreign('asset_unit_id')
                    ->references('id')
                    ->on('demands')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demand_asset_units');
    }
}
